<div id="searchform" class="">
     <div class="backclose"></div>
     <div class="container">
          <div class="row full justify-content-center">
               <div class="col-12 col-lg-8 align-self-center">
                    <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                         <input type="text" class="field" name="s" id="s" placeholder="Buscar" />
                         <input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/img/IClupa.png" value="Pesquisar" id="lupa-icone" />
                    </form>
               </div>
          </div>
     </div>
</div>