<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>
     <section class="clinica">
          <div class="container">
               <div class="row full justify-content-center">

                    <div class="col-12 col-md-10 col-lg-9">
                         <div class="aclinica">
                              <h2 class="titulo type sameline"><?php charspan('Sobre a FIV São Paulo'); ?></h2>
                              <p>FIV SP acredita que ciência não significa distância! A medicina deve ser baseada em evidências, lógico, mas o acolhimento e personalização devem andar de mãos dadas.</p>

                              <p>Para isso, contamos com um corpo médico multidisciplinar e experiente, onde o comprometimento com o resultado da paciente/casal é medido pela incansável dedicação à ciência, tecnologia, ética médica e tratamento personalizado.</p>

                              <p>Entendemos que cada ser humano é único e merece todo suporte e cuidado em cada etapa da busca pela realização do sonho de constituir uma família.</p>

                              <p>Médicos, embriologista, enfermeiras, psicólogas e nutricionistas garantem uma medicina não somente individualizada, mas sim personalizada, isto é, pensada e direcionada para a resolução dos desafios enfrentados pelo casal, buscando a melhor opção possível oferecida pela medicina reprodutiva, seja na forma presencial ou virtualmente (como as ferramentas que a telemedicina nos oferece).</p>

                              <p>O laboratório de reprodução humana, é o coração de uma clínica de medicina reprodutiva, é nele onde são feitos os procedimentos de micromanipulação dos óvulos e espermatozoide assim como o desenvolvimento embrionário nas estufas. Para se ter uma ideia, é controlado o número de partículas no ar dentro do laboratório, tudo para extrair a melhor performance de uma tecnologia de ponta.</p>

                              <p>Pensando nos casais que recebemos de fora do país, ou até mesmos de outros estados brasileiros, estamos estrategicamente localizados ao lado da Av. Paulista, a mais importante avenida da metrópole paulistana. Próximo ao aeroporto de Congonhas, e ao lado de shoppings centers e uma vasta variedade de hotéis e restaurantes, nossa localização torna a experiência das pacientes de fora de SP algo excepcional.</p>
                         </div>
                    </div>

               </div>
          </div>
     </section>
<?php get_footer();
