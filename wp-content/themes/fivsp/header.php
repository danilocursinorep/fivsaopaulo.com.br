<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FIV_São_Paulo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-X53ZSLEKC4"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-X53ZSLEKC4');
	</script>	
	<!-- Global site tag (gtag.js) - Google Ads: 341939908 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-341939908"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'AW-341939908');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '499008041201611');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=499008041201611&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<?php if (is_page('agendamento-obrigado')): ?>
	<!-- Event snippet for FIV SP - Reservar horário conversion page -->
	<script>
		gtag('event', 'conversion', {'send_to': 'AW-341939908/l9NOCPmOyNQCEMSthqMB'});
	</script>
	<?php elseif (is_page('contato-obrigado')): ?>
	<!-- Event snippet for FIV SP - Contato conversion page -->
	<script>
		gtag('event', 'conversion', {'send_to': 'AW-341939908/AQ4rCJ7FydQCEMSthqMB'});
	</script>		
	<?php endif; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#DA532C">
	<meta name="theme-color" content="#FFFFFF">	

	<?php wp_head(); ?>
</head>

<?php if(is_page()): ?>
	<?php global $post; ?>
	<?php $slug = $post->post_name; ?>
<?php else: ?>
	<?php $slug = ''; ?>
<?php endif; ?>

<body <?php body_class($slug); ?>>

<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<?php  get_template_part( 'template-parts/menu', 'flutuante' ); ?>
	</header><!-- #masthead -->

	<main id="primary" class="site-main">
