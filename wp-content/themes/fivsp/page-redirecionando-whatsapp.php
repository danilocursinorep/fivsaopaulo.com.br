<!doctype html>
<html <?php language_attributes(); ?>>
     <head>
          <!-- Global site tag (gtag.js) - Google Analytics -->
          <script async src="https://www.googletagmanager.com/gtag/js?id=G-X53ZSLEKC4"></script>
          <script>
               window.dataLayer = window.dataLayer || [];
               function gtag(){dataLayer.push(arguments);}
               gtag('js', new Date());

               gtag('config', 'G-X53ZSLEKC4');
          </script> 
          <!-- Global site tag (gtag.js) - Google Ads: 341939908 -->
          <script async src="https://www.googletagmanager.com/gtag/js?id=AW-341939908"></script>
          <script>
               window.dataLayer = window.dataLayer || [];
               function gtag(){dataLayer.push(arguments);}
               gtag('js', new Date());

               gtag('config', 'AW-341939908');
          </script>
          <!-- Facebook Pixel Code -->
          <script>
               !function(f,b,e,v,n,t,s)
               {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
               n.callMethod.apply(n,arguments):n.queue.push(arguments)};
               if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
               n.queue=[];t=b.createElement(e);t.async=!0;
               t.src=v;s=b.getElementsByTagName(e)[0];
               s.parentNode.insertBefore(t,s)}(window, document,'script',
               'https://connect.facebook.net/en_US/fbevents.js');
               fbq('init', '499008041201611');
               fbq('track', 'PageView');
          </script>
          <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=499008041201611&ev=PageView&noscript=1"
          /></noscript>
          <!-- End Facebook Pixel Code -->
          <!-- Event snippet for Contato WhatsApp conversion page -->
          <script>
               gtag('event', 'conversion', {'send_to': 'AW-341939908/QOHlCKmtj9UCEMSthqMB'});
          </script>     
          <meta charset="<?php bloginfo( 'charset' ); ?>">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="profile" href="https://gmpg.org/xfn/11">
          <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
          <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
          <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
          <meta name="msapplication-TileColor" content="#DA532C">
          <meta name="theme-color" content="#FFFFFF">  
          <?php wp_head(); ?>
     </head>
     <body <?php body_class(); ?>>
          <?php wp_body_open(); ?>
          <div id="page" class="site">
               <main id="primary" class="site-main">

                    <section class="obrigado wp">
                         <div class="container">
                              <div class="row justify-content-center">
                                   <div class="col-12 col-lg-6 text-center align-self-center">
                                        <div class="conteudo">
                                             <img class="load" src="<?php echo get_template_directory_uri(); ?>/assets/img/load.gif">
                                             <h2>Você será redirecionado para o WhatsApp<br> da <strong>FIV São Paulo</strong> em <strong class="contador">10</strong> segundos</h2>
                                             <h3>Caso, não sejá redirecionado, <a href="https://wa.me/5511994537042/">clique aqui</a>.</h3>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>

               </main><!-- #main -->
          </div><!-- #page -->
          <?php wp_footer(); ?>
          <script type="text/javascript">
               $(document).ready(function(){
                    var intervalo = 9;
                    setInterval(function(){
                         $('.contador').html(intervalo);
                         intervalo--;
                         if (intervalo == -1) {
                              window.location.href = 'https://wa.me/5511994537042';
                         }
                    }, 1000);
               });
          </script>
     </body>
</html>
