<?php get_header(); ?>

	<section class="obrigado ag">
          <div class="container">

               <div class="row">
                    <div class="col-12 col-lg-3 offset-lg-1 align-self-center">
                         <div class="conteudo">
                              <h2>Recebemos seus dados para o pré-agendamento da consulta!</h2>
                              <h3><strong>ATENÇÃO:</strong> Seu pré-agendamento <em>não garante</em> a efetivação da consulta. </h3>
                              <h4>Uma de nossas secretárias irá entrar em contato com você para confirmar o melhor dia e horário para a sua consulta.</h4>
                              <h5>Em caso de dúvida, por favor, entre em contato.</h5>
                              <ul>
                                   <li>
                                        <a class="whatstrigger">
                                             <img src="<?php echo get_template_directory_uri(); ?>/assets/img/miniWhatsapp.png">(11) <strong>99453-7042</strong>
                                        </a>
                                   </li>
                                   <li>
                                        <a target="_blank" href="tel:551147501763">
                                             <img src="<?php echo get_template_directory_uri(); ?>/assets/img/miniTelefone.png">(11) <strong>4750-1763</strong>
                                        </a>
                                   </li>
                              </ul>
                         </div>
                    </div>
			</div>
		</div>

	</main><!-- #main -->

<?php get_footer();