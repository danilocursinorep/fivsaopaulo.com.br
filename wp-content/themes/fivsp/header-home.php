<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FIV_São_Paulo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-X53ZSLEKC4"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-X53ZSLEKC4');
	</script>	
	<!-- Global site tag (gtag.js) - Google Ads: 341939908 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-341939908"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'AW-341939908');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '499008041201611');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=499008041201611&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#DA532C">
	<meta name="theme-color" content="#FFFFFF">	

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="home-header">
		<img data-enllax-ratio="0.1" data-enllax-type="foreground" class="imgBG header fade top" src="<?php echo get_template_directory_uri(); ?>/assets/img/headerBG.png">
		<img data-enllax-ratio="0.2" data-enllax-type="foreground" class="imgBG ball small" src="<?php echo get_template_directory_uri(); ?>/assets/img/ball.png">
		<?php get_template_part( 'template-parts/menu', 'flutuante' ); ?>
		<div class="conteudoHeader">
			<div class="container">
				<div class="row full">
					<div class="col-12 col-lg-6 align-self-center">
						<h2 class="type">
							<?php charspan('Porque '); ?>
							<strong>
								<?php charspan('ciência'); ?>
							</strong>
							<?php charspan('não significa'); ?>
							<strong>
								<?php charspan('distância'); ?>
							</strong>
						</h2>
                         	<div class="linha fade left"></div>
						<a href="<?php echo home_url('agendamento'); ?>">
							<button class="fade left"><span>agende sua consulta</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/BTseta.png" /></button>
						</a>
					</div>
					<!-- <div class="col-12 col-lg-6 align-self-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ball.png">
					</div> -->
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<main id="primary" class="site-main">
