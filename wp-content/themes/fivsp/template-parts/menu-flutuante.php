<?php get_search_form(); ?>
<div class="menuFlutuante">
	<div class="container-fluid fade top">
		<div class="row">
			<div class="col-6 col-sm-8 col-md-3 col-lg-2 order-2 order-md-1 align-self-center site-branding">
				<a id="logo" class="" target="_self" href="<?php echo home_url(); ?>">
					
					<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logosm.png" />

					<span class="sp">
						<span>São Paulo</span>
						<span>sp</span>
					</span>
				</a>
			</div><!-- .site-branding -->
			<nav id="site-navigation" class="main-navigation col-3 col-sm-2 col-md-7 col-lg-8 order-1 order-md-2 align-self-md-center">
                   	<!-- <div id="menu-icone">
                       <span></span>
                       <span></span>
                       <span></span>
                       <span></span>
                  	</div> -->
                  	<button id="menu-button">Menu</button>
				<div class="menu-container">
					<?php wp_nav_menu(array(
						'theme_location' => 'menu-1',
						'menu_id' => 'primary-menu'
					)); ?>
				</div>
			</nav><!-- #site-navigation -->
			<div class="col-3 col-sm-2 order-3 align-self-center social text-right">
				<div class="row">
					<div class="d-none d-lg-block col-lg-9">
						<div class="conteudo">
							<a target="_blank" href="https://www.facebook.com/FIV-S%C3%A3o-Paulo-101438382104879/">
								<img class="rede facebook" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICFace.png" />
							</a>
							<a target="_blank" href="https://www.instagram.com/fivsaopaulo/">
								<img class="rede instagram" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICInsta.png" />
							</a>
							<a class="whatstrigger">
								<img class="rede whatsapp" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICWhats.png" />
							</a>
							<a target="_blank" href="https://goo.gl/maps/mWS2JEXLa9Asve4j8">
								<img class="rede map" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICMaps.png" />
							</a>
						</div>
					</div>
					<div class="col-12 col-lg-3">
						<div class="pesquisa">
							<img class="rede lupa" src="<?php echo get_template_directory_uri(); ?>/assets/img/IClupa.png" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>