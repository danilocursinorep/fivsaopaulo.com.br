<?php get_header(); ?>

     <section class="ebooksInterna">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="titulo col-12 col-md-10 col-lg-9 text-right">
                         <h2>E-books</h2>
                    </div>
               <?php while(have_posts()): the_post(); ?>
                    <?php if (!isset($_GET['obrigado'])): ?>
                         <div class="ebook col-12 col-md-10 col-lg-9 align-self-center">
                              <div class="row content">
                                   <div class="col-12 col-lg-6">
                                        <img class="img" src="<?php the_post_thumbnail_url(); ?>" />
                                   </div>
                                   <div class="col-12 col-lg-6 align-self-center">
                                        <div class="conteudo">
                                             <h2><?php the_title(); ?></h2>
                                             <?php the_field('descricao_curta'); ?>
                                        </div>
                                   </div>
                                   <div class="desc col-12 col-lg-6">
                                        <?php the_field('descricao'); ?>
                                   </div>
                                   <div class="form col-12 col-lg-6 align-self-center">
                                        <h3>Preencha seus dados para o download do e-book:</h3>
                                        <?php the_field('formulario_full'); ?>
                                        <?php the_field('formulario_script'); ?>
                                   </div>
                              </div>
                         </div>
                    <?php else: ?>
                         <div class="ebook col-12 col-md-10 col-lg-9 align-self-center">
                              <div class="row content justify-content-center">
                                   <div class="col-12 col-lg-6">
                                        <img class="img" src="<?php the_post_thumbnail_url(); ?>" />
                                   </div>
                                   <div class="col-12 col-lg-6 align-self-center">
                                        <div class="conteudo">
                                             <h2><?php the_title(); ?></h2>
                                             <?php the_field('descricao_curta'); ?>
                                        </div>
                                   </div>
                                   <div class="obg col-12 col-lg-10">
                                        <h2>Obrigado</h2>

                                        <p>Logo você receberá seu e-book no e-mail cadastrado.</p>

                                        <p>Com esse e-book em suas mãos, esperamos te auxiliar no esclarecimento de dúvidas sobre diversos assuntos da reprodução assistida.</p>
                                        <p>Sempre teremos novos e-books para te manter informada e ajudá-la da melhor forma possível apresentando as muitas possibilidades que a medicina reprodutiva disponibilza atualmente para a realização do sonho da maternidade.</p>
                                        <p>Surgindo novas questões, não deixe de entrar em contato.</p>

                                        <a href="<?php echo home_url('agendamento'); ?>">
                                             <button>Agendar consulta</button>
                                        </a>


                                   </div>
                              </div>
                         </div>
                    <?php endif; ?>
               <?php endwhile; ?>


                         <div class="posts col-12 col-md-10 col-lg-9 align-self-center">
                              <?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
                              <?php $queryPosts = new WP_Query(array( 
                                   'post_type' => 'ebook',
                                   'post__not_in' => array(get_the_ID()),
                                   'posts_per_page' => 1,
                                   'orderby' => 'rand',
                              )); ?>

                              <?php if($queryPosts->have_posts()): ?>
                                   <?php while($queryPosts->have_posts()): $queryPosts->the_post(); ?>
                                   <a href="<?php the_permalink(); ?>" class="row post">
                                        <div class="col-12 col-lg-6 align-self-center">
                                             <div class="conteudo">
                                                  <h2><?php the_title(); ?></h2>
                                                  <?php echo get_excerpt(120); ?>
                                                  <button>Baixe agora</button>
                                             </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                             <img class="img" src="<?php the_post_thumbnail_url(); ?>" />
                                        </div>
                                   </a>
                                   <?php endwhile; ?>
                              <?php endif; ?>   

                         </div>
                    </div>

               </div>
          </div>
     </section>
<?php get_footer();
