<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>
     <section class="ebooks">
          <div class="container">

          <div class="row justify-content-center">

               <div class="titulo col-12 col-md-10 col-lg-9 text-right">
                    <h2>E-books</h2>
               </div>
               <div class="posts col-12 col-md-10 col-lg-9 align-self-center">
                    <?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
                    <?php $queryPosts = new WP_Query(array( 
                         'post_type' => 'ebook',
                         'posts_per_page' => 3,
                         'order' => 'DESC',
                         'paged' => $paged 
                    )); ?>
                    <?php if($queryPosts->have_posts()): ?>
                         <?php while($queryPosts->have_posts()): $queryPosts->the_post(); ?>
                         <a href="<?php the_permalink(); ?>" class="row post">
                              <div class="col-12 col-lg-6">
                                   <img class="img" src="<?php the_post_thumbnail_url(); ?>" />
                              </div>
                              <div class="col-12 col-lg-6 align-self-center">
                                   <div class="conteudo">
                                        <h2><?php the_title(); ?></h2>
                                        <p><?php the_field('descricao_mini'); ?></p>
                                        <button>Baixe agora</button>
                                   </div>
                              </div>
                         </a>
                         <?php endwhile; ?>
                    <?php endif; ?>   
                    <div class="row justify-content-center">
                         <div class="paginacao col-12">
                              <?php pagination($queryPosts); ?>
                         </div>
                    </div>
               </div>
          </div>

          </div>
     </section>
<?php get_footer();
