<?php get_header(); ?>

     <?php while(have_posts()): the_post(); ?>
     <section class="ebooksInterna">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="ebook col-12 col-md-10 col-lg-9 align-self-center">
                         <a href="<?php the_permalink(); ?>" class="row content">
                              <div class="col-12 col-lg-6">
                                   <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                              </div>
                              <div class="col-12 col-lg-6 align-self-center">
                                   <div class="conteudo">
                                        <h2><?php the_title(); ?></h2>
                                        <?php the_field('descricao_curta'); ?>
                                   </div>
                              </div>
                              <div class="col-12 col-lg-6">
                                   <?php the_field('descricao'); ?>
                              </div>
                              <div class="col-12 col-lg-6 align-self-center">
                                   <h3>Preencha seus dados para o download do e-book:</h3>
                                   <?php the_field('formulario_full'); ?>
                                   <?php the_field('formulario_script'); ?>
                              </div>
                         </a>
                    </div>


                    <div class="posts col-12 col-md-10 col-lg-9 align-self-center">
                         <?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
                         <?php $queryPosts = new WP_Query(array( 
                              'post_type' => 'ebook',
                              'post__not_in' => array(get_the_ID()),
                              'posts_per_page' => 1,
                              'order' => 'rand',
                              'paged' => $paged 
                         )); ?>
                         <?php if($queryPosts->have_posts()): ?>
                              <?php while($queryPosts->have_posts()): $queryPosts->the_post(); ?>
                              <a href="<?php the_permalink(); ?>" class="row post">
                                   <div class="col-12 col-lg-6">
                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                   </div>
                                   <div class="col-12 col-lg-6 align-self-center">
                                        <div class="conteudo">
                                             <h2><?php the_title(); ?></h2>
                                             <?php echo get_excerpt(120); ?>
                                             <button>Baixe agora</button>
                                        </div>
                                   </div>
                              </a>
                              <?php endwhile; ?>
                         <?php endif; ?>   

                    </div>
               </div>
          </div>
     </section>
     <?php endwhile; ?>
<?php get_footer();
