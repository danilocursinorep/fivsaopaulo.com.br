<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>
     <section class="equipe">
          <div class="container">

               <div class="row full justify-content-center">

                    <div class="col-12 col-md-10 col-lg-9">
                         <h2 class="titulo type sameline">
                              <?php charspan('conheça'); ?>
                              <br>
                              <?php charspan('nossa'); ?>
                              <strong>
                                   <?php charspan('equipe'); ?>
                              </strong>
                         </h2>
                    </div>

               <?php $queryEquipe = new WP_Query(array( 
                    'post_type' => 'equipe',
                    'posts_per_page' => -1,
                    'order' => 'DESC',
                    'orderby' => 'rand'
               )); ?>
               <?php if($queryEquipe->have_posts()): ?>

                    <div class="equipe col-12 col-md-10 col-lg-9 align-self-center">

                         <?php while($queryEquipe->have_posts()): $queryEquipe->the_post(); ?>
                         <div class="membro row">
                              <div class="info col-12 col-md-6 align-self-center">
                                   <h2><?php the_title(); ?></h2>
                                   <h3><?php the_field('crm'); ?> <?php echo (get_field('rqe'))?' | '.get_field('rqe'):''; ?></h3>
                                   <h4><?php the_field('cargo'); ?></h4>
                                   <div class="descricao">
                                        <?php the_field('descricao_curta'); ?>
                                        <?php if (get_field('descricao')): ?>
                                             <div class="mais">
                                                  <?php the_field('descricao'); ?>
                                             </div>
                                             <button class="saibamais">Saiba mais</button>
                                        <?php endif; ?>
                                   </div>
                              </div>
                              <div class="thumb col-12 col-md-6">
                                   <?php the_post_thumbnail(); ?>
                              </div>
                         </div>
                         <?php endwhile; ?>

                    </div>

               <?php endif; ?>
             </div>

        </div>
     </section>
<?php get_footer();
