<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>

	<section class="page404">
          <div class="container">

               <div class="row full justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9 align-self-center">

					<h2 class="titulo">Página não encontrada!</h2>
					<h3 class="titulo">Tente buscar o conteúdo desejado pesaquisando abaixo.</h3>
					
					<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	                         <input type="text" class="field" name="s" id="s" placeholder="Buscar" />
	                         <input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/img/IClupa.png" value="Pesquisar" id="lupa-icone" />
	                    </form>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();