<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>

	<?php while (have_posts()): the_post(); ?>
     <section class="single">
          <div class="container">

               <div class="row full justify-content-center">
               	<div class="post col-12 col-md-10 col-lg-9">
		               <div class="row full justify-content-center">
		                    <div class="destaque col-12">
		                    	<div class="topo" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
		                    </div>
		                    <div class="conteudos col-12 col-lg-10">
		                    	<h2><?php the_title(); ?></h2>
		                    	<h3>Por <?php the_author(); ?></h3>
		                    	<div class="conteudo">
		                    		<?php the_content(); ?>
		                    	</div>
		                    </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; ?>


     <?php $queryPost = new WP_Query(array( 
          'post_type' => 'post',
          'posts_per_page' => 3,
          'order' => 'DESC'
     )); ?>
     <?php if($queryPost->have_posts()): ?>
     <section class="blogHome sing">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">

                         <div class="itens row">
                         <?php while($queryPost->have_posts()): $queryPost->the_post(); ?>
                              <div class="item col-12 col-md-4">
                                   <a href="<?php the_permalink(); ?>" class="conteudo">
                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        <div class="detalhes">
                                             <h3><?php the_title(); ?></h3>
                                             <?php echo get_excerpt(120); ?>
                                        </div>
                                   </a>
                              </div>
                         <?php endwhile; ?>
                         </div>

                    </div>
               </div>

          </div>
     </section>
     <?php endif; ?>

<?php get_footer();