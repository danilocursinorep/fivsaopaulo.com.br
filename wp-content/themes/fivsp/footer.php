<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FIV_São_Paulo
 */

?>

</main><!-- #main -->
<footer id="colophon" class="site-footer">
  <div class="container">
    <div class="row">
      <div class="site-info col-12 col-lg-6">
        <a target="_self" href="<?php echo home_url(); ?>">
          <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" />
        </a>
        <a target="_blank" href="tel:551147501763">
          <span>11 4750.1763</span>
        </a>
        <a class="whatstrigger">
          <span>11 99453.7042</span>
        </a>
        <a target="_blank" href="https://goo.gl/maps/mWS2JEXLa9Asve4j8">
          <span class="endereco">Rua Cincinato Braga 37<br> 9° andar • Conjunto 92<br /> Bela Vista • São Paulo SP<br />
            CEP 01333-011</span>
        </a>
      </div>
      <div class="social col-12 col-lg-6">
        <a target="_blank" href="https://www.facebook.com/FIV-S%C3%A3o-Paulo-101438382104879/">
          <img class="rede facebook" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICFace.png" />
        </a>
        <a target="_blank" href="https://www.instagram.com/fivsaopaulo/">
          <img class="rede instagram" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICInsta.png" />
        </a>
        <a class="whatstrigger">
          <img class="rede whatsapp" src="<?php echo get_template_directory_uri(); ?>/assets/img/ICWhats.png" />
        </a>
      </div>

      <div class="creditos col-12">
        <span><?php echo date('Y'); ?> © Todos os direitos reservados. O conteúdo deste site foi elaborado pela equipe
          da FIV São Paulo e as informações aqui contidas tem caráter meramente informativo e educacional. Não deve ser
          utilizado para realizar autodiagnóstico ou automedicação. Em caso de dúvidas, consulte seu médico, somente ele
          está habilitado a praticar o ato médico, conforme recomendação do Conselho Federal de Medicina. Todas imagens
          contidas no site são meramente ilustrativas e foram compradas em banco de imagens, não envolvendo imagens de
          pacientes. Diretor Técnico Responsável: Dr. Leopoldo de Oliveira Tso • CRM-SP 97267 • RQE 65256.</span>
      </div>

    </div>
  </div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
<script type="text/javascript">
if (typeof MauticSDKLoaded == 'undefined') {
  var MauticSDKLoaded = true;
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://mkt.fivsaopaulo.com.br/media/js/mautic-form.js';
  script.onload = function() {
    MauticSDK.onLoad();
  };
  head.appendChild(script);
  var MauticDomain = 'https://mkt.fivsaopaulo.com.br';
  var MauticLang = {
    'submittingMessage': "Por favor, aguarde..."
  }
} else if (typeof MauticSDK != 'undefined') {
  MauticSDK.onLoad();
}
</script>

</html>