<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package FIV_São_Paulo
*/
get_header(); ?>
<section class="agendemendo">
     <div class="container">
          <div class="row full justify-content-center">
               <div class="col-12 col-md-7 align-self-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/agendamento.png">
               </div>
               <div class="col-12 col-md-4 align-self-center">
                    <h2 class="type">
                    <?php charspan('Agende sua'); ?>
                    <strong>
                    <?php charspan('consulta'); ?>
                    </strong>
                    </h2>
                    <p>Preencha seus dados e a nossa equipe entrará em contato para confirmar o agendamento.</p>
                    <div id="mauticform_wrapper_agendamentosite" class="mauticform_wrapper">
                         <form autocomplete="false" role="form" method="post" action="https://mkt.fivsaopaulo.com.br/form/submit?formId=1" id="mauticform_agendamentosite" data-mautic-form="agendamentosite" enctype="multipart/form-data">
                              <div class="mauticform-error" id="mauticform_agendamentosite_error"></div>
                              <div class="mauticform-message" id="mauticform_agendamentosite_message"></div>
                              <div class="mauticform-innerform">
                                   
                                   <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">
                                        <div id="mauticform_agendamentosite_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
                                             <input id="mauticform_input_agendamentosite_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
                                             <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                        </div>
                                        <div id="mauticform_agendamentosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
                                             <input id="mauticform_input_agendamentosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
                                             <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                        </div>
                                        <div id="mauticform_agendamentosite_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
                                             <input id="mauticform_input_agendamentosite_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input celnum" type="text">
                                             <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                        </div>
                                        <div id="mauticform_agendamentosite_modalidade" data-validate="modalidade" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-4 mauticform-required">
                                             <select id="mauticform_input_agendamentosite_modalidade" name="mauticform[modalidade]" value="" placeholder="Tipo de Consulta" class="mauticform-selectbox">
                                                  <option value="" disabled selected>Tipo de Consulta</option>                    <option value="Presencial">Presencial</option>                    <option value="Telemedicina">Telemedicina (on-line)</option>
                                             </select>
                                             <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                        </div>
                                        <div id="mauticform_agendamentosite_medico" data-validate="medico" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-5 mauticform-required">
                                             <select id="mauticform_input_agendamentosite_medico" name="mauticform[medico]" value="" placeholder="Especialista" class="mauticform-selectbox capitalize">
                                                  <option value="" disabled selected>Especialista</option>
                                                  <option value="Indiferente">Indiferente</option>
                                                  <?php
                                                  $medicos = Array(
                                                  '<option value="Dr. Cristiano Eduardo Busso">Dr. Cristiano Eduardo Busso</option>',
                                                  '<option value="Dra. Karina Silveira Leite Tafner">Dra. Karina Silveira Leite Tafner</option>',
                                                  '<option value="Dr. Leopoldo de Oliveira Tso">Dr. Leopoldo de Oliveira Tso</option>',
                                                  '<option value="Dr. Newton Eduardo Busso">Dr. Newton Eduardo Busso</option>',
                                                  '<option value="Dr. Rodrigo Sabato Romano">Dr. Rodrigo Sabato Romano</option>'
                                                  );
                                                  shuffle($medicos);
                                                  foreach ($medicos as $medico) {
                                                  echo $medico;
                                                  }
                                                  ?>
                                             </select>
                                             <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                        </div>
                                        <div id="mauticform_agendamentosite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-6">
                                             <button type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">Pré-agendar</button>
                                        </div>
                                   </div>
                                   <input type="hidden" name="mauticform[formId]" id="mauticform_agendamentosite_id" value="1">
                                   <input type="hidden" name="mauticform[return]" id="mauticform_agendamentosite_return" value="">
                                   <input type="hidden" name="mauticform[formName]" id="mauticform_agendamentosite_name" value="agendamentosite">
                              </form>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</section>
<?php get_footer();