<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>

	<section class="page">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">
				<?php while (have_posts()): ?>
					<?php the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php endwhile; ?>
                    </div>
			</div>
		</div>

	</main><!-- #main -->

<?php get_footer();