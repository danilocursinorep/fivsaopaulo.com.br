<?php get_header(); ?>

	<section class="obrigado co">
          <div class="container">

               <div class="row">
                    <div class="col-12 col-lg-3 offset-lg-1 align-self-center">
                         <div class="conteudo">
                              <h2>Muito obrigado, recebemos sua mensagem.</h2>
                              <h4>Uma de nossas secretárias entrará em contato em breve com você.</h4>
                              <h5>Em caso de dúvida, por favor, entre em contato.</h5>
                              <ul>
                                   <li>
                                        <a class="whatstrigger">
                                             <img src="<?php echo get_template_directory_uri(); ?>/assets/img/miniWhatsapp.png">(11) <strong>99453-7042</strong>
                                        </a>
                                   </li>
                                   <li>
                                        <a target="_blank" href="tel:551147501763">
                                             <img src="<?php echo get_template_directory_uri(); ?>/assets/img/miniTelefone.png">(11) <strong>4750-1763</strong>
                                        </a>
                                   </li>
                              </ul>
                         </div>
                    </div>
			</div>
		</div>

<?php get_footer();