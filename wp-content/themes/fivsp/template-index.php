<?php //Template Name: Página de Índice ?>
<?php get_header(); ?>

	<section class="index">
          <div class="container">

               <div class="row full justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9 align-self-start">
					<?php
						$args = array(
						    'post_type' => 'page',
						    'posts_per_page' => -1,
						    'post_parent' => $post->ID,
						    'order' => 'ASC',
						    'orderby' => 'name'
						 );
					 ?>
					<?php $parent = new WP_Query($args); ?>
					<?php if ($parent->have_posts()): ?>

						<h2 class="titulo"><?php the_title(); ?></h2>




						<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

							<?php if(has_post_thumbnail()): ?>
		                              <a href="<?php the_permalink(); ?>" class="item row">
		                                   <div class="col-12 col-md-4 align-self-center">
		                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
		                                   </div>
		                                   <div class="col-12 col-md-8 align-self-center">
	                                             <h2><?php the_title(); ?></h2>
	                                             <?php get_excerpt(220); ?>
		                                   </div>
		                              </a>
		                         <?php else: ?>
		                              <a href="<?php the_permalink(); ?>" class="item big row">
		                                   <div class="col-12 text-center align-self-center">
	                                             <h2><?php the_title(); ?></h2>
		                                   </div>
		                              </a>
		                         <?php endif; ?>

						<?php endwhile; ?>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();