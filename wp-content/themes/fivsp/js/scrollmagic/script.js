var dir = document.URL.substr(0,document.URL.lastIndexOf('/')) + '/wp-content/themes/fivsp/';
var obj = {curImg: 0};

//Animação "FIV"
var fiv = [
     dir + "assets/img/especialidades/fiv/icsi_001.jpg",
     dir + "assets/img/especialidades/fiv/icsi_002.jpg",
     dir + "assets/img/especialidades/fiv/icsi_003.jpg",
     dir + "assets/img/especialidades/fiv/icsi_004.jpg",
     dir + "assets/img/especialidades/fiv/icsi_005.jpg",
     dir + "assets/img/especialidades/fiv/icsi_006.jpg",
     dir + "assets/img/especialidades/fiv/icsi_007.jpg",
     dir + "assets/img/especialidades/fiv/icsi_008.jpg",
     dir + "assets/img/especialidades/fiv/icsi_009.jpg",
     dir + "assets/img/especialidades/fiv/icsi_010.jpg",
     dir + "assets/img/especialidades/fiv/icsi_011.jpg",
     dir + "assets/img/especialidades/fiv/icsi_012.jpg",
     dir + "assets/img/especialidades/fiv/icsi_013.jpg",
     dir + "assets/img/especialidades/fiv/icsi_014.jpg",
     dir + "assets/img/especialidades/fiv/icsi_015.jpg",
     dir + "assets/img/especialidades/fiv/icsi_016.jpg",
     dir + "assets/img/especialidades/fiv/icsi_017.jpg",
     dir + "assets/img/especialidades/fiv/icsi_018.jpg",
     dir + "assets/img/especialidades/fiv/icsi_019.jpg",
     dir + "assets/img/especialidades/fiv/icsi_020.jpg",
     dir + "assets/img/especialidades/fiv/icsi_021.jpg",
     dir + "assets/img/especialidades/fiv/icsi_022.jpg",
     dir + "assets/img/especialidades/fiv/icsi_023.jpg",
     dir + "assets/img/especialidades/fiv/icsi_024.jpg",
     dir + "assets/img/especialidades/fiv/icsi_025jpgg",
     dir + "assets/img/especialidades/fiv/icsi_026.jpg",
     dir + "assets/img/especialidades/fiv/icsi_027.jpg",
     dir + "assets/img/especialidades/fiv/icsi_028.jpg",
     dir + "assets/img/especialidades/fiv/icsi_029.jpg",
     dir + "assets/img/especialidades/fiv/icsi_030.jpg",
     dir + "assets/img/especialidades/fiv/icsi_031.jpg",
     dir + "assets/img/especialidades/fiv/icsi_032.jpg",
     dir + "assets/img/especialidades/fiv/icsi_033.jpg",
     dir + "assets/img/especialidades/fiv/icsi_034.jpg",
     dir + "assets/img/especialidades/fiv/icsi_035.jpg",
     dir + "assets/img/especialidades/fiv/icsi_036.jpg",
     dir + "assets/img/especialidades/fiv/icsi_037.jpg",
     dir + "assets/img/especialidades/fiv/icsi_038.jpg",
     dir + "assets/img/especialidades/fiv/icsi_039.jpg",
     dir + "assets/img/especialidades/fiv/icsi_040.jpg",
     dir + "assets/img/especialidades/fiv/icsi_041.jpg",
     dir + "assets/img/especialidades/fiv/icsi_042.jpg"];
var tweenFIV = TweenMax.to(obj, 0.5, {
          curImg: fiv.length - 1,
          roundProps: "curImg",
          immediateRender: true,
          ease: Linear.easeNone,
          onUpdate: function () {
            $("#fiv .anima img").attr("src", fiv[obj.curImg]);
          }
     }
);
var controllerFIV = new ScrollMagic.Controller();
var scene = new ScrollMagic.Scene({
     triggerElement: "#fiv",
     duration: ($("#fiv .anima").height() - 200),
     triggerHook: "onCenter",
     reverse: true
})
.setTween(tweenFIV)
.addTo(controllerFIV);

//Animação "Preservação da Fertilidade"
var pre = [
     dir + "assets/img/especialidades/preservacao/preservacao_00000.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00001.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00002.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00003.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00004.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00005.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00006.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00007.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00008.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00009.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00010.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00011.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00012.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00013.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00014.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00015.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00016.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00017.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00018.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00019.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00020.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00021.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00022.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00023.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00024.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00025jpgg",
     dir + "assets/img/especialidades/preservacao/preservacao_00026.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00027.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00028.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00029.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00030.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00031.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00032.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00033.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00034.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00035.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00036.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00037.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00038.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00039.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00040.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00041.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00042.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00043.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00044.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00045.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00046.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00047.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00048.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00049.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00050.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00051.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00052.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00053.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00054.jpg",
     dir + "assets/img/especialidades/preservacao/preservacao_00055.jpg"
];
var tweenPRE = TweenMax.to(obj, 0.5, {
          curImg: pre.length - 1,
          roundProps: "curImg",
          immediateRender: true,
          ease: Linear.easeNone,
          onUpdate: function () {
            $("#preservacao .anima img").attr("src", pre[obj.curImg]);
          }
     }
);
var controllerPRE = new ScrollMagic.Controller();
var scene = new ScrollMagic.Scene({
     triggerElement: "#preservacao",
     duration: $("#preservacao").height(),
     triggerHook: "onCenter",
     reverse: true
})
.setTween(tweenPRE)
.addTo(controllerPRE);

//Animação "Tratamento de Endometriose"
var end = [
     dir + "assets/img/especialidades/endometriose/endometriose_00000.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00001.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00002.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00003.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00004.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00005.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00006.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00007.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00008.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00009.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00010.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00011.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00012.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00013.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00014.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00015.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00016.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00017.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00018.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00019.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00020.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00021.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00022.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00023.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00024.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00025jpgg",
     dir + "assets/img/especialidades/endometriose/endometriose_00026.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00027.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00028.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00029.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00030.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00031.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00032.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00033.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00034.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00035.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00036.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00037.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00038.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00039.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00040.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00041.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00042.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00043.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00044.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00045.jpg",
     dir + "assets/img/especialidades/endometriose/endometriose_00046.jpg"
];
var tweenEND = TweenMax.to(obj, 0.5, {
          curImg: end.length - 1,
          roundProps: "curImg",
          immediateRender: true,
          ease: Linear.easeNone,
          onUpdate: function () {
            $("#endometriose .anima img").attr("src", end[obj.curImg]);
          }
     }
);
var controllerEND = new ScrollMagic.Controller();
var scene = new ScrollMagic.Scene({
     triggerElement: "#endometriose",
     duration: $("#endometriose").height(),
     triggerHook: "onCenter",
     reverse: true
})
.setTween(tweenEND)
.addTo(controllerEND);

//Animação "Telemedicina"
var tel = [
     dir + "assets/img/especialidades/telemedicina/telemedicina_01.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_02.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_03.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_04.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_05.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_06.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_07.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_08.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_09.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_010.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_011.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_012.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_013.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_014.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_015.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_016.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_017.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_018.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_019.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_020.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_021.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_022.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_023.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_024.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_025.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_026.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_027.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_028.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_029.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_030.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_031.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_032.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_033.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_034.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_035.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_036.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_037.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_038.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_039.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_040.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_041.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_042.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_043.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_044.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_045.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_046.png",
     dir + "assets/img/especialidades/telemedicina/telemedicina_047.png",
];
var tweenTEL = TweenMax.to(obj, 0.5, {
          curImg: tel.length - 1,
          roundProps: "curImg",
          immediateRender: true,
          ease: Linear.easeNone,
          onUpdate: function () {
            $("#telemedicina .anima img").attr("src", tel[obj.curImg]);
          }
     }
);
var controllerTEL = new ScrollMagic.Controller();
var scene = new ScrollMagic.Scene({
     triggerElement: "#telemedicina",
     duration: $("#telemedicina").height(),
     triggerHook: "onCenter",
     reverse: true
})
.setTween(tweenTEL)
.addTo(controllerTEL);