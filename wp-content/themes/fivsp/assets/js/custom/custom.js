function menu() {

     if ($(window).scrollTop() > 30)  {
          $('.menuFlutuante').addClass('open');
          $('#logo').addClass('short');
          $('.menuFlutuante').addClass('show');
     }  else {
          $('.menuFlutuante').removeClass('open');
          $('#logo').removeClass('short');
          $('.menuFlutuante').removeClass('show');
     }

}

$(window).scroll(function(){
     menu();
});

$(document).ready(function(){
     menu();
     
     $('.menuFlutuante .social .lupa').on('click', function(){
          $('#searchform').addClass('open');
          setTimeout(function() {
               $('#searchform').addClass('show');
          }, 1);
          $('#searchform input[type=text]').focus();
     });
     
     $('#searchform .backclose').on('click', function(){
          $('#searchform').removeClass('show');
          setTimeout(function() {
               $('#searchform').removeClass('open');
          }, 400);
     });
     
     $('#menu-icone').on('click', function(){
          $(this).toggleClass('open');
          if ($('#site-navigation .menu-container').hasClass('show')) {
               $('#site-navigation .menu-container').removeClass('show');
               setTimeout(function() {
                    $('#site-navigation .menu-container').removeClass('open');
               }, 400);
          } else {
               $('#site-navigation .menu-container').addClass('open');
               setTimeout(function() {
                    $('#site-navigation .menu-container').addClass('show');
               }, 1);
          }
     });

     $('.sub-menu > li.menu-item-has-children > a, .menu > li.menu-item-has-children > a').on('click touchstart', function(e){
          e.preventDefault();
          var sm = $(this).next('.sub-menu');
          var pa = $(this).parent();
          var gr = $(this).parent().parent();
          if (!pa.hasClass('show')) {
               gr.children('.menu-item').not(pa).removeClass('show');
               gr.children('.menu-item').children('.sub-menu').not(sm).removeClass('show');
               gr.children('.menu-item').children('.sub-menu').not(sm).removeClass('open');
               sm.addClass('open');
               pa.addClass('show');
               setTimeout(function() {
                    sm.addClass('show');
               }, 1);
          } else {
               sm.removeClass('show');
               sm.removeClass('open');
               pa.removeClass('show');
          }
     });

     var anima = 0;
     $('.type').on('inview', function(event, isInView) {
          if (!$(this).hasClass('fadein')) {
               $(this).addClass('fadein');
               var fade = $(this).addClass('fadein' + anima);
               let animation = anime({
                   targets: '.fadein'+ anima +' span',
                   translateX: [0,-30],
                   opacity: [0,1],
                   easing: "easeInExpo",
                   duration: 1000,
                   delay: (el, i) => 100 + 30 * i
               });
               anima++;
          }
     });
     $('.fade:not(.modal)').on('inview', function(event, isInView) {
          if (!$(this).hasClass('fadein')) {
               $(this).addClass('fadein');
               var fade = $(this).addClass('fadein' + anima);

               if($(this).hasClass('top')) {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateY: [-30,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1200
                    });
               } else if($(this).hasClass('left')) {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateX: [-30,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1750
                    });
               } else if($(this).hasClass('right')) {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateX: [30,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1750
                    });
               } else {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateY: [100,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1200
                    });
               }
               anima++;
          }
          
     });
     $('.ball').on('inview', function(event, isInView) {
          var $this = $(this);
          if (!$(this).hasClass('fadein')) {
               $(this).addClass('fadein');
               setTimeout(function() {
                    $this.removeClass('small');
               }, 100);
          }
     });


     $('.celnum').mask('(00) 00000-0000');

     $('.mauticform-selectbox').on('change', function(){
          if ($(this).val()) {
               $(this).addClass('valor');
          } else {
               $(this).removeClass('valor');
          }
     });

     $('#menu-button').click(function(){
          $(this).toggleClass('open');
          if ($('#site-navigation .menu-container').hasClass('show')) {
               $('#site-navigation .menu-container').removeClass('show');
               setTimeout(function() {
                    $('#site-navigation .menu-container').removeClass('open');
               }, 400);
          } else {
               $('#site-navigation .menu-container').addClass('open');
               setTimeout(function() {
                    $('#site-navigation .menu-container').addClass('show');
               }, 1);
          }
     });

     $('#mauticform_checkboxgrp_checkbox_gostaria_de_preagendar_um1_Gostariadepreagendarumaconsulta0').on('change', function(){
          if ($(this).is(':checked')) {
               $('#mauticform_ebookteste_qual_o_dia_da_semana_de_p, #mauticform_ebookteste_qual_o_periodo_de_prefere').addClass('show');
          } else {
               $('#mauticform_ebookteste_qual_o_dia_da_semana_de_p, #mauticform_ebookteste_qual_o_periodo_de_prefere').removeClass('show');
          }
     });

     $('.equipe .equipe .membro .saibamais').on('click', function(){
          var mais = $(this).prev('.mais');
          mais.toggleClass('show');
          $(this).toggleClass('menos');

          if($(this).hasClass('menos')) {
               $(this).html('Mostrar menos');
          } else {
               $(this).html('Saiba mais');
          }

     });
     
});