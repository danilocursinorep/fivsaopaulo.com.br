<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>
     <section class="contato">
          <div class="container">

               <div class="row full justify-content-center">
                    <div class="col-12 col-md-6 col-lg-5 offset-md-5 offset-lg-5 align-self-center">
                         <div class="contatoform fade right">
                              <h2 class="type">
                                   <?php charspan('Entre em'); ?>
                                   <strong>
                                        <?php charspan('contato'); ?>
                                   </strong>
                              </h2>

                              <div class="item">
                                   <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/iconePin.png">
                                   <a target="_blank" href="https://goo.gl/maps/mWS2JEXLa9Asve4j8">
                                        <p>Rua Cincinato Braga 37<br> 9° andar • Conjunto 92<br/> Bela Vista  • São Paulo SP<br/> CEP 01333-011</span></p>
                                   </a>
                              </div>

                              <div class="item">
                                   <img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeTelefone.png">
                                   <p>
                                        <a target="_blank" href="tel:551147501763">
                                             <span>11 4750.1763</span>
                                        </a><br>
                                        <a class="whatstrigger">
                                             <span>11 99453.7042</span>
                                        </a>
                                   </p>
                              </div>
                              <div id="mauticform_wrapper_contatosite" class="mauticform_wrapper">
                                  <form autocomplete="false" role="form" method="post" action="https://mkt.fivsaopaulo.com.br/form/submit?formId=2" id="mauticform_contatosite" data-mautic-form="contatosite" enctype="multipart/form-data">
                                      <div class="mauticform-error" id="mauticform_contatosite_error"></div>
                                      <div class="mauticform-message" id="mauticform_contatosite_message"></div>
                                      <div class="mauticform-innerform">

                                          
                                        <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

                                          <div id="mauticform_contatosite_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
                                              <input id="mauticform_input_contatosite_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
                                              <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                          </div>

                                          <div id="mauticform_contatosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
                                              <input id="mauticform_input_contatosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
                                              <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                          </div>

                                          <div id="mauticform_contatosite_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
                                              <input id="mauticform_input_contatosite_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input celnum" type="text">
                                              <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                          </div>

                                          <div id="mauticform_contatosite_mensagem" class="mauticform-row mauticform-text mauticform-field-4">
                                              <textarea id="mauticform_input_contatosite_mensagem" name="mauticform[mensagem]" placeholder="Mensagem" class="mauticform-textarea"></textarea>
                                              <span class="mauticform-errormsg" style="display: none;"></span>
                                          </div>

                                          <div id="mauticform_contatosite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
                                              <button type="submit" name="mauticform[submit]" id="mauticform_input_contatosite_submit" value="" class="mauticform-button btn btn-default">Enviar</button>
                                          </div>
                                          </div>
                                      </div>

                                      <input type="hidden" name="mauticform[formId]" id="mauticform_contatosite_id" value="2">
                                      <input type="hidden" name="mauticform[return]" id="mauticform_contatosite_return" value="">
                                      <input type="hidden" name="mauticform[formName]" id="mauticform_contatosite_name" value="contatosite">

                                      </form>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </section>
<?php get_footer();