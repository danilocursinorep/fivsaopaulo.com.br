<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package FIV_São_Paulo
 */

get_header(); ?>

	<section class="search">
          <div class="container">

               <div class="row full justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9 align-self-start">


					<?php if ( have_posts() ) : ?>

						<h2 class="titulo">Resultado da pesquisa</h2>
						<h3 class="titulo">para as palavras-chaves '<?php echo get_search_query(); ?>'.</h3>
						
						<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		                         <input type="text" class="field" name="s" id="s" placeholder="<?php echo get_search_query(); ?>" value="<?php echo get_search_query(); ?>" />
		                         <input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/img/IClupa.png" value="Pesquisar" id="lupa-icone" />
		                    </form>

						<?php while (have_posts()): the_post(); ?>


							<?php if (get_post_type() == 'page'): ?>
		                              <a href="<?php the_permalink(); ?>" class="item row">
		                                   <div class="col-12 align-self-center text-center">
		                                   	<h3>Página</h3>
	                                             <h2><?php the_title(); ?></h2>
		                                   </div>
		                              </a>
	                              <?php elseif (get_post_type() == 'post'): ?>
		                              <a href="<?php the_permalink(); ?>" class="item row">
		                                   <div class="col-12 col-md-4 align-self-center">
		                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
		                                   </div>
		                                   <div class="col-12 col-md-8 align-self-center">
	                                             <h3>Blogpost</h3>
	                                             <h2><?php the_title(); ?></h2>
	                                             <?php get_excerpt(220); ?>
		                                   </div>
		                              </a>
	                              <?php elseif (get_post_type() == 'ebook'): ?>
		                              <a href="<?php the_permalink(); ?>" class="item row">
		                                   <div class="col-12 col-md-4 align-self-center">
		                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
		                                   </div>
	                                   	<div class="col-12 col-md-8 align-self-center">
	                                             <h3>E-book</h3>
	                                             <h2><?php the_title(); ?></h2>
	                                             <?php get_excerpt(220); ?>
		                                   </div>
		                              </a>
	                              <?php endif; ?>

						<?php endwhile; ?>

					<?php else: ?>

						<h2 class="titulo">Nada encontrado</h2>
						<h3 class="titulo">A pesquisa para '<?php echo get_search_query(); ?>' não retornou nenhum resutado.</h3>
						
						<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		                         <input type="text" class="field" name="s" id="s" placeholder="Buscar" />
		                         <input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/img/IClupa.png" value="Pesquisar" id="lupa-icone" />
		                    </form>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();