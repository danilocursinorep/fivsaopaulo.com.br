<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package FIV_São_Paulo
 */

get_header('home'); ?>

     <section id="fiv" class="especialidade">
          <div class="anima">
               <img src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/fiv/icsi_001.jpg" />
          </div>
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12 col-lg-8 align-self-center">
                         <a href="<?php echo home_url('fertilizacao-in-vitro-fiv'); ?>">
                              <h2 class="type">
                                   <?php charspan('Fertilização'); ?>
                                   <strong>
                                        <?php charspan('in vitro'); ?>
                                   </strong>
                              </h2>
                              <p class="fade bottom">A FIV é a técnica da medicina reprodutiva mais adotada no mundo por apresentar as maiores taxas de sucesso para os tratamentos de casais inférteis. Também é uma alternativa para casais homoafetivos que desejam ter um filho.</p>
                              <button class="fade bottom">Saiba mais</button>
                         </a>
                    </div>
               </div>
          </div>
     </section>
    
     <section id="preservacao" class="especialidadealt">
          <div class="container">
               <div class="sticky">
                    <div class="row justify-content-center">
                         <div class="txt fade left col-12 col-md-6 col-lg-5 align-self-center text-right">
                              <a href="<?php echo home_url('preservacao-social-da-fertilidade'); ?>">
                                   <h2 class="type">
                                        <?php charspan('Preservação da'); ?>
                                        <strong>
                                             <?php charspan('fertilidade') ?>
                                        </strong>
                                   </h2>
                                   <p class="fade left">Alternativa para as pessoas que desejam planejar a gravidez para o futuro, adiando o desejo de ter um filho para um momento mais oportuno, por motivos profissionais, pessoais ou financeiros.</p>
                                   <button class="fade bottom">Saiba mais</button>
                              </a>
                         </div>
                         <div class="anima fade right col-12 col-md-4 col-lg-7 align-self-center">
                              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/preservacao/preservacao_00001.jpg" />
                         </div>
                    </div>
               </div>
          </div>
     </section>
    
     <section id="endometriose" class="especialidadealt">
          <div class="container">
               <div class="sticky">
                    <div class="row justify-content-center">
                         <div class="anima fade right col-12 col-md-4 col-lg-7 align-self-center">
                              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/endometriose/endometriose_00001.jpg" />

                         </div>
                         <div class="fade left col-12 col-md-6 col-lg-5 align-self-center">
                              <a href="<?php echo home_url('endometriose'); ?>">
                                   <h2 class="type">
                                        <?php charspan('Tratamento de'); ?>
                                        <strong>
                                             <?php charspan('endometriose'); ?>
                                        </strong>
                                   </h2>
                                   <p class="fade bottom">A endometriose é uma das doenças femininas mais comuns, atingindo um percentual alto de mulheres em idade fértil. Além de afetar profundamente a qualidade de vida da paciente, é a principal causa da infertilidade feminina. Seu tratamento é fundamental para o sucesso da gravidez.</p>
                                   <button class="fade bottom">Saiba mais</button>
                              </a>
                         </div>
                    </div>
               </div>
          </div>
     </section>
    
     <section id="telemedicina" class="especialidadealt">
          <div class="container">
               <div class="sticky">
                    <div class="row justify-content-center">
                         <div class="txt fade left col-12 col-md-6 col-lg-5 align-self-center text-right">
                              <a href="<?php echo home_url('agendamento'); ?>">
                                   <h2 class="type"><?php charspan('Telemedicina') ?></h2>
                                   <p class="fade left">Seu atendimento também à distância, com expertise, dedicação e acolhimento. Uma alternativa para etapas de diversos tratamentos que não necessitam de uma investigação presencial, realizada com o paciente no conforto de seu lar.</p>
                                   <button class="fade bottom">Agende sua teleconsulta</button>
                              </a>
                         </div>
                         <div class="anima fade right col-12 col-md-4 col-lg-5 align-self-center">
                              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/telemedicina/telemedicina_01.png" />
                         </div>
                    </div>
               </div>
          </div>
     </section>

     <?php /* <section id="preservacao" class="especialidade">
          <div class="anima">
          </div>
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12 col-md-4 col-lg-8 align-self-center">
                         <a href="<?php echo home_url('preservacao-social-da-fertilidade'); ?>">
                              <h2 class="type">
                                   <?php charspan('Preservação da'); ?>
                                   <strong>
                                        <?php charspan('fertilidade') ?>
                                   </strong>
                              </h2>
                              <p class="fade left">A criopreservação de gametas e embriões por motivos médicos, como tratamentos de câncer ou cirurgia de endometriose, já é uma alternativa explorada há tempos (...)</p>
                         </a>
                    </div>
               </div>
          </div>
     </section>

     <section id="endometriose" class="especialidade">
          <div class="anima">
               <img src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/endometriose/endometriose_00001.jpg" />
          </div>
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12 col-md-4 col-lg-8 align-self-center">
                         <a href="<?php echo home_url('endometriose'); ?>">
                              <h2 class="type">
                                   <?php charspan('Tratamento de'); ?>
                                   <strong>
                                        <?php charspan('endometriose'); ?>
                                   </strong>
                              </h2>
                              <p class="fade bottom">A endometriose é uma doença associada a manifestações dolorosas, infertilidade feminina e outros prejuízos à qualidade de vida da mulher. O quadro caracteriza-se (...)</p>
                         </a>
                    </div>
               </div>
          </div>
     </section>

    <section class="especialidade">
          <div class="container">
               <div id="preservacao" class="fade especialidade row justify-content-center">
                    <div class="col-12 col-md-6 col-lg-5 align-self-center">
                         <a href="<?php echo home_url('preservacao-social-da-fertilidade'); ?>">
                              <h2 class="type">
                                   <?php charspan('Preservação da'); ?>
                                   <strong>
                                        <?php charspan('fertilidade') ?>
                                   </strong>
                              </h2>
                              <p class="fade left">A criopreservação de gametas e embriões por motivos médicos, como tratamentos de câncer ou cirurgia de endometriose, já é uma alternativa explorada há tempos (...)</p>
                         </a>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 align-self-center">
                         <img class="fade right" src="<?php echo get_template_directory_uri(); ?>//assets/img/preservacao.png">
                    </div>
               </div>
          </div>
     </section>

     <section class="especialidade">
          <div class="container">
               <div id="endometriose" class="fade especialidade row justify-content-center">
                    <div class="col-12 col-md-6 col-lg-4 align-self-center">
                         <img class="fade right" src="<?php echo get_template_directory_uri(); ?>//assets/img/endometriose.png">
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 align-self-center">
                         <a href="<?php echo home_url('endometriose'); ?>">
                              <h2 class="type">
                                   <?php charspan('Tratamento de'); ?>
                                   <strong>
                                        <?php charspan('endometriose'); ?>
                                   </strong>
                              </h2>
                              <p class="fade left">A endometriose é uma doença associada a manifestações dolorosas, infertilidade feminina e outros prejuízos à qualidade de vida da mulher. O quadro caracteriza-se (...)</p>
                         </a>
                    </div>
               </div>
          </div>
     </section> */ ?>

     <section class="empresa fade">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8 align-self-center">
                         <h2 class="type sameline">
                              <?php charspan('fiv'); ?>
                              <strong>
                                   <?php charspan('são paulo'); ?>
                              </strong>
                         </h2>

                         <div class="linha fade left"></div>

                         <img class="equipe fade right" src="<?php echo get_template_directory_uri(); ?>//assets/img/equipe.png">
                         <p class="fade left">FIV SP acredita que ciência não significa distância! A medicina deve ser baseada em evidências, lógico, mas o acolhimento e personalização devem andar de mãos dadas.</p>

                         <p class="fade left">Para isso, contamos com um corpo médico multidisciplinar e experiente, onde o comprometimento com o resultado da paciente/casal é medido pela incansável dedicação à ciência, tecnologia, ética médica e tratamento personalizado.</p>
                    </div>
               </div>

          </div>
     </section>
     
     <section class="agendamentoHome fade">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="col-12 col-md-6 col-lg-4">
                         <img class="fade left" src="<?php echo get_template_directory_uri(); ?>//assets/img/mapa.png">
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                         <h2 class="type">
                              <?php charspan('Agende sua'); ?>
                              <strong>
                                   <?php charspan('consulta'); ?>
                              </strong>
                         </h2>
                         <div id="mauticform_wrapper_agendamentosite" class="mauticform_wrapper">
                              <form autocomplete="false" role="form" method="post" action="https://mkt.fivsaopaulo.com.br/form/submit?formId=1" id="mauticform_agendamentosite" data-mautic-form="agendamentosite" enctype="multipart/form-data">
                                   <div class="mauticform-error" id="mauticform_agendamentosite_error"></div>
                                   <div class="mauticform-message" id="mauticform_agendamentosite_message"></div>
                                   <div class="mauticform-innerform">
                                        
                                        <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">
                                             <div id="mauticform_agendamentosite_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
                                                  <input id="mauticform_input_agendamentosite_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
                                                  <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                             </div>
                                             <div id="mauticform_agendamentosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
                                                  <input id="mauticform_input_agendamentosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
                                                  <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                             </div>
                                             <div id="mauticform_agendamentosite_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
                                                  <input id="mauticform_input_agendamentosite_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input celnum" type="text">
                                                  <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                             </div>
                                             <div id="mauticform_agendamentosite_modalidade" data-validate="modalidade" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-4 mauticform-required">
                                                  <select id="mauticform_input_agendamentosite_modalidade" name="mauticform[modalidade]" value="" placeholder="Tipo de Consulta" class="mauticform-selectbox">
                                                       <option value="" disabled selected>Tipo de Consulta</option>                    <option value="Presencial">Presencial</option>                    <option value="Telemedicina">Telemedicina (on-line)</option>
                                                  </select>
                                                  <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                             </div>
                                             <div id="mauticform_agendamentosite_medico" data-validate="medico" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-5 mauticform-required">
                                                  <select id="mauticform_input_agendamentosite_medico" name="mauticform[medico]" value="" placeholder="Especialista" class="mauticform-selectbox capitalize">
                                                       <option value="" disabled selected>Especialista</option>
                                                       <option value="Indiferente">Indiferente</option>
                                                       <?php
                                                       $medicos = Array(
                                                       '<option value="Dr. Cristiano Eduardo Busso">Dr. Cristiano Eduardo Busso</option>',
                                                       '<option value="Dra. Karina Silveira Leite Tafner">Dra. Karina Silveira Leite Tafner</option>',
                                                       '<option value="Dr. Leopoldo de Oliveira Tso">Dr. Leopoldo de Oliveira Tso</option>',
                                                       '<option value="Dr. Newton Eduardo Busso">Dr. Newton Eduardo Busso</option>',
                                                       '<option value="Dr. Rodrigo Sabato Romano">Dr. Rodrigo Sabato Romano</option>'
                                                       );
                                                       shuffle($medicos);
                                                       foreach ($medicos as $medico) {
                                                       echo $medico;
                                                       }
                                                       ?>
                                                  </select>
                                                  <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
                                             </div>
                                             <div id="mauticform_agendamentosite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-6">
                                                  <button type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">Pré-agendar</button>
                                             </div>
                                        </div>
                                        <input type="hidden" name="mauticform[formId]" id="mauticform_agendamentosite_id" value="1">
                                        <input type="hidden" name="mauticform[return]" id="mauticform_agendamentosite_return" value="">
                                        <input type="hidden" name="mauticform[formName]" id="mauticform_agendamentosite_name" value="agendamentosite">
                                   </form>
                              </div>
                         </div>
                         <p class="fade right">Rua Cincinato Braga, 37<br/>9° andar • Conjunto 92<br/> Bela Vista • São Paulo SP <br/>CEP 01333-011</p>
                    </div>
               </div>

          </div>
     </section>

     <?php $queryPost = new WP_Query(array( 
          'post_type' => 'post',
          'posts_per_page' => 3,
          'order' => 'DESC'
     )); ?>
     <?php if($queryPost->have_posts()): ?>
     <section class="blogHome fade">
          <div class="container">

               <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">
                         <h2 class="type"><?php charspan('Últimas '); ?><strong><?php charspan('postagens'); ?></strong></h2>
                    </div>
                    <div class="col-12 col-md-10 col-lg-9">
                         <div class="itens row">

                         <?php while($queryPost->have_posts()): $queryPost->the_post(); ?>
                              <div class="item col-12 col-md-4">
                                   <a href="<?php the_permalink(); ?>" class="conteudo">
                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        <div class="detalhes">
                                             <div class="h3">
                                                  <h3><?php the_title(); ?></h3>
                                             </div>
                                             <?php get_excerpt(120); ?>
                                        </div>
                                   </a>
                              </div>
                         <?php endwhile; ?>

                         </div>
                    </div>
               </div>
          </div>
     </section>
     <?php endif; ?>

     <?php $queryBook = new WP_Query(array( 
          'post_type' => 'ebook',
          'posts_per_page' => 2,
          'order' => 'DESC'
     )); ?>
     <?php if($queryBook->have_posts()): ?>
     <section class="ebooksHome fade">
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">
                         <h2 class="type"><?php charspan('E-books'); ?></h2>
                    </div>
                    <div class="col-12 col-md-10 col-lg-9">
                         <?php while($queryBook->have_posts()): $queryBook->the_post(); ?>
                              <a href="<?php the_permalink(); ?>" class="item row">
                                   <div class="col-12 col-md-4 align-self-center">
                                        <div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                   </div>
                                   <div class="col-12 col-md-8 align-self-center">
                                        <div class="detalhes">
                                             <h2>e-book</h2>
                                             <h3><?php the_title(); ?></h3>
                                             <p><?php the_field('descricao_mini'); ?></p>
                                        </div>
                                   </div>
                              </a>
                         <?php endwhile; ?>
                    </div>
               </div>
          </div>
     </section>
     <?php endif; ?>
<?php get_footer();